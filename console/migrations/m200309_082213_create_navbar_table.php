<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%navbar}}`.
 */
class m200309_082213_create_navbar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%navbar}}', [
            'id' => $this->primaryKey(),
            'nav_block_name' => $this->string(),
            'nav_block_url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%navbar}}');
    }
}
