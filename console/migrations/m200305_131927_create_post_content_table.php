<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_content}}`.
 */
class m200305_131927_create_post_content_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_content}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(),
            'content' => $this->text(),
            'type' => $this->string(),
            'label' => $this->text(),
            'image' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post_content}}');
    }
}
