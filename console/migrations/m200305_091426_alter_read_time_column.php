<?php

use yii\db\Migration;

/**
 * Class m200305_091426_alter_read_time_column
 */
class m200305_091426_alter_read_time_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('post', 'read_time', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200305_091426_alter_read_time_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200305_091426_alter_read_time_column cannot be reverted.\n";

        return false;
    }
    */
}
