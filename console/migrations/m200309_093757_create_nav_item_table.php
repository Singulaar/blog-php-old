<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%nav_item}}`.
 */
class m200309_093757_create_nav_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%nav_item}}', [
            'id' => $this->primaryKey(),
            'nav_item_id' => $this->integer(),
            'nav_item_content' => $this->string(),
            'nav_item_type' => $this->string(),
            'nav_item_img' => $this->string(),
            'nav_item_url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%nav_item}}');
    }
}
