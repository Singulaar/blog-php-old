<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%terms}}`.
 */
class m200310_121234_create_terms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%terms}}', [
            'id' => $this->primaryKey(),
            'content' => $this->text(),
            'type' => $this->string(),
            'label' => $this->string(),
            'image' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%terms}}');
    }
}
