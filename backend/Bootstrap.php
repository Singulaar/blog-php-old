<?php

namespace backend;

use mrstroz\wavecms\components\helpers\FontAwesome;
use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {

        Yii::$app->params['nav']['authors'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Authors',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'author',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Authors List',
                    'url' => ['/author/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['contact'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Contact page',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'contact',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Contact',
                    'url' => ['/contact/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['footer'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Footer',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'footer',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Footer elements',
                    'url' => ['/footer/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['navbar'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Navigation',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'navbar',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Navbar Items',
                    'url' => ['/navbar/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['posts'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Post',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'post',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Posts content',
                    'url' => ['/post/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['category'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Post category',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'category',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Category',
                    'url' => ['/category/index']
                ],
            ]
        ];

        Yii::$app->params['nav']['terms'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Terms page',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'terms',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'Terms content',
                    'url' => ['/terms/index']
                ],
            ]
        ];

    }
}