<?php

namespace backend\controllers;

use frontend\models\Terms;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class TermsController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Terms');

        $this->query = Terms::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            'type',
            'content',
            'image',
            'label',
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}