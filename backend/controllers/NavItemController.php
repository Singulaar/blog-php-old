<?php

namespace backend\controllers;

use frontend\models\Navitem;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class NavItemController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Navigation Item');

        $this->query = NavItem::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            'nav_item_type',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'nav_item_content',
            ],
            'attribute' => 'nav_item_img',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'nav_item_url',
            ],
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}