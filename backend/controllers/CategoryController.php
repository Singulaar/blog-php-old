<?php

namespace backend\controllers;

use frontend\models\Category;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class CategoryController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Post category');

        $this->query = Category::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            'category',
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}