<?php

namespace backend\controllers;

use frontend\models\Author;
use frontend\models\Post;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class PostController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Posts');

        $this->query = Post::find()->select([
            'post.id',
            'post.title',
            'post.short_description',
            'post.read_time',
            'post.img',
            'post.post_meta',
            'post.author_id',
            'post.status',
            'author' => 'author.author_name',
            'post.create_time',
            'post.update_time',
        ])->leftJoin(Author::tableName(), Post::tableName() . '.author_id = ' . Author::tableName() . '.id');

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            [
                'attribute' => 'author',
                'value' => 'author_id'
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'title',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'short_description',
            ],
            'read_time',
            'img',
            'post_meta',
            'status',
            'create_time',
            'update_time',
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}