<?php

namespace backend\controllers;

use frontend\models\PostContent;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class PostContentController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Post contents');

        $this->query = PostContent::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            'post_id',
            'type',
            'content',
            'image',
            'label',
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}