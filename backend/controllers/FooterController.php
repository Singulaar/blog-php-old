<?php

namespace backend\controllers;

use frontend\models\Footer;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class FooterController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Footer');

        $this->query = Footer::find()->select([
            'footer.id',
            'footer.type',
            'footer.content',
            'footer.url',
        ]);

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            'type',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'content',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'url',
            ],
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}