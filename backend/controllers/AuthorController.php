<?php

namespace backend\controllers;

use frontend\models\Author;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class AuthorController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Authors');

        $this->query = Author::find()->select([
            'author.id',
            'author.author_name',
            'author.author_meta',
            'author.author_pic',
        ]);

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'author_name',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'author_meta',
            ],
            'author_pic',
            [
                'class' => ActionColumn::className(),
            ]
        );
    }

}