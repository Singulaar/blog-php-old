<?php

namespace backend\controllers;

use frontend\models\Navbar;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class NavbarController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Navigation');

        $this->query = Navbar::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'nav_block_name',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'nav_block_url',
            ],
            [
                'class' => ActionColumn::className(),
            ]
            );
    }

}