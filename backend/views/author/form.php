<?php

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\ImageWidget;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Authors'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'author_name')->textInput();
            echo $form->field($model, 'author_meta')->textInput();
            echo $form->field($model, 'author_pic')->widget(ImageWidget::class);
            ?>

            <?php PanelWidget::end(); ?>

        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>