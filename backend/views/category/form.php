<?php

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Category'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'category')->textInput();

            ?>

            <?php PanelWidget::end(); ?>
        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>