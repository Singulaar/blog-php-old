<?php

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Footer'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'type')->dropDownList(['(not set)' => '(not set)', 'menu-link' => 'menu-link', 'footer-about' => 'footer-about', 'footer-nav-element' => 'footer-nav-element']);
            echo $form->field($model, 'content')->textInput();
            echo $form->field($model, 'url')->textInput();

            ?>

            <?php PanelWidget::end(); ?>

        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>