<?php

/* @var $authorList array */

use frontend\models\Author;
use frontend\models\Category;
use kartik\datetime\DateTimePicker;
use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\ImageWidget;
use mrstroz\wavecms\components\widgets\PanelWidget;
use mrstroz\wavecms\components\widgets\SubListWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Posts'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'title')->textInput();
            echo $form->field($model, 'short_description')->textInput();
            echo $form->field($model, 'read_time')->textInput(['type' => 'number']);
            echo $form->field($model, 'post_meta')->dropDownList(Category::categoryDropdown());
            echo $form->field($model, 'img')->widget(ImageWidget::class);
            echo $form->field($model, 'author_id')->dropDownList(Author::nameDropdown());
            echo $form->field($model, 'status')->dropDownList([0 => 'No', 1 => 'YES']);
            echo $form->field($model, 'create_time')->widget(DateTimePicker::className(),[
                'value' => $model->create_time,
                'pluginOptions' => [
                    'autoclose'=>true,
                ]
            ]);
            echo $form->field($model, 'update_time')->hiddenInput(['value' => date('Y-m-d H:m:s')])->label(false);

            ?>
            <?php PanelWidget::end(); ?>

        </div>

        <div class="col-md-6">
            <?= SubListWidget::widget([
                'model' => $model,
                'listId' => 'post_content'
            ]); ?>
        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>