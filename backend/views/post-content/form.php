<?php

/* @var $authorList array */

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\CKEditorWidget;
use mrstroz\wavecms\components\widgets\ImageWidget;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Posts'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'type')->dropDownList(['(not set)' => '(not set)', 'text' => 'text', 'image-big' => 'image_big', 'quote' => 'quote', 'tip' => 'tip']);
            echo $form->field($model, 'content')->widget(CKEditorWidget::class);
            echo $form->field($model, 'label')->textInput();
            echo $form->field($model, 'image')->widget(ImageWidget::class);

            ?>

            <?php PanelWidget::end(); ?>
        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>