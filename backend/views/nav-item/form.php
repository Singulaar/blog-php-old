<?php

use frontend\models\Navbar;
use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\ImageWidget;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Navigation Item'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'nav_item_id')->dropDownList(Navbar::navigationIdDropdown());
            echo $form->field($model, 'nav_item_content')->textInput();
            echo $form->field($model, 'nav_item_type')->dropDownList(['(not set)' => '(not set)', 'text' => 'text', 'image' => 'image', 'url' => 'url']);
            echo $form->field($model, 'nav_item_img')->widget(ImageWidget::class);
            echo $form->field($model, 'nav_item_url')->textInput(['placeholder' => '']);

            ?>

            <?php PanelWidget::end(); ?>

        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>