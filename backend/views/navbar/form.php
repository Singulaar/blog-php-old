<?php

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\PanelWidget;
use mrstroz\wavecms\components\widgets\SubListWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Navigation'), 'panel_class' => 'panel-primary']);

            echo $form->field($model, 'nav_block_name')->textInput();
            echo $form->field($model, 'nav_block_url')->textInput(['value' => '#']);
            ?>

            <?php PanelWidget::end(); ?>

        </div>

        <div class="col-md-6">
            <?= SubListWidget::widget([
                'model' => $model,
                'listId' => 'nav_item'
            ]); ?>
        </div>

    </div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>