<?php

/* @var $socials TermsNavbarWidget */

use common\widgets\TermsNavbarWidget;
use yii\helpers\Url;

?>
<header class="header ">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <div class="navbar-brand">
                <a class="navbar-logo" href="<?= Url::home() ?>">
                    <img class="navbar-pic" src="<?= Url::to(['img/parrot.png']) ?>" width="88" alt="Parrot">
                </a>
                <a class="navbar-type" href="<?= Url::home() ?>">Blog</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown"
                    aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'terms') {
                            echo "active";
                        } ?>" href="<?= Url::to(['site/terms']) ?>">Terms of Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'privacy') {
                            echo "active";
                        } ?>" href="#">Privacy Policy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'license') {
                            echo "active";
                        } ?>" href="#">License</a>
                    </li>
                </ul>
                <div class="socials">
                    <?php foreach ($socials as $block): ?>
                        <a class="socials-item" href="<?= $block->nav_item_url ?>">
                            <img class="socials-pic"
                                 src="http://localhost/advanced/frontend/web/images/<?= $block->nav_item_content ?>"
                                 alt="<?= $block->nav_item_content ?>">
                        </a>
                    <?php endforeach; ?>
                </div>
        </nav>
    </div>
</header>