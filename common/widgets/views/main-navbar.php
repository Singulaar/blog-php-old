<?php

/* @var $nav NavbarWidget */
/* @var $post NavbarWidget */
/* @var $nav_item NavbarWidget */

/* @var $socials NavbarWidget */

use common\widgets\NavbarWidget;
use yii\helpers\Url;

?>
<header class="header js-header-post">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <div class="navbar-brand">
                <a class="navbar-logo" href="<?= Url::home() ?>">
                    <img class="navbar-pic" src="<?= Url::to(['img/parrot.png']) ?>" width="88" alt="Parrot">
                </a>
                <a class="navbar-type" href="<?= Url::home() ?>">Blog</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown"
                    aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarDropdown">
                <ul class="navbar-nav">
                    <?php foreach ($nav as $block): ?>
                        <?php if ($block->nav_block_name != 'Socials'): ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link<?php if ($block->navItems) {
                                    echo ' dropdown-toggle';
                                } ?>" id="navbarDropdownLink" href="<?= $block->nav_block_url ?>" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false"><?= $block->nav_block_name ?></a>
                                <div class="<?php if ($block->navItems) {
                                    echo ' dropdown-menu';
                                } ?>" aria-labelledby="navbarDropdownLink">
                                    <?php foreach ($block->navItems as $nav_item): ?>
                                        <a class="dropdown-item <?php if (Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == $nav_item->nav_item_url) {
                                            echo "active";
                                        } ?>"
                                           href="<?php echo Url::to([$nav_item->nav_item_url]); ?>"><?= $nav_item->nav_item_content ?></a>
                                    <?php endforeach; ?>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <?php if (Yii::$app->controller->id == 'post' && Yii::$app->controller->action->id == 'index'): ?>
                    <?php foreach ($post
                                   as $item): ?>

                        <div class="header-article"><?= $item->title ?></div>
                    <?php endforeach; ?>
                    <div class="share">
                        <div class="share-title">Spread your love, <strong>share the article</strong></div>
                        <div class="share-list">
                            <a href="#" class="share-btn share-twitter btn">
                                <img class="share-icon"
                                     src="http://localhost/advanced/frontend/web/img/icon-twitter-white.svg"
                                     alt="Share on Twitter">
                                <span class="share-label">Share on Twitter</span>
                            </a>
                            <a href="#" class="share-btn share-facebook btn">
                                <img class="share-icon"
                                     src="http://localhost/advanced/frontend/web/img/icon-facebook-white.svg"
                                     alt="Share on Facebook">
                                <span class="share-label">Share on Facebook</span>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="socials">
                    <?php foreach ($socials as $block): ?>
                        <a class="socials-item" href="<?= $block->nav_item_url ?>">
                            <img class="socials-pic"
                                 src="http://localhost/advanced/frontend/web/images/<?= $block->nav_item_content ?>"
                                 alt="<?= $block->nav_item_content ?>">
                        </a>
                    <?php endforeach; ?>
                </div>

            </div>
        </nav>
    </div>

    <div class="indicator">
        <div class="indicator-position js-indicator-position"></div>
    </div>

</header>

