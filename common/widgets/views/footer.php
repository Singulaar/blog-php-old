<?php

/* @var $footer FooterWidget */

/* @var $socials FooterWidget */

use common\widgets\FooterWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>

<div class="menu">
    <div class="menu-list">
        <?php foreach ($footer as $block): ?>
            <?php if ($block->type === 'menu-link'): ?>
                <a class="menu-link" href="<?php echo Url::to([$block->url]); ?>"><?= $block->content ?></a>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>

<footer class="footer

<?php
if (Yii::$app->controller->id == 'post' && Yii::$app->controller->action->id == 'index') {echo "post-footer";}
?>">

    <div class="container">
        <?= Html::a('Parrot', ['site/index'], ['class' => 'footer-logo']) ?>
        <?php foreach ($footer as $block): ?>
            <?php if ($block->type === 'footer-about'): ?>
                <div class="footer-about"><?= $block->content ?></div>
            <?php endif; ?>
        <?php endforeach; ?>

        <div class="subscribe">
            <?php $form = ActiveForm::begin(); ?>
            <div class="subscribe-form">
                <div class="form-group">
                    <input class="form-control" type="email" placeholder="Enter your email" required="">
                </div>
                <?= Html::submitButton('Subscribe', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <div class="subscribe-content"><strong>No spam.</strong> New guides, posts, tools, and boosts. Get on board!
            </div>
        </div>

        <div class="socials">
            <?php foreach ($socials as $block): ?>
                <a class="socials-item" href="<?php echo Url::to([$block->nav_item_url]); ?>">
                    <img class="socials-pic"
                         src="http://localhost/advanced/frontend/web/images/<?= $block->nav_item_content ?>"
                         alt="<?= $block->nav_item_content ?>">
                </a>
            <?php endforeach; ?>
        </div>

        <nav class="footer-nav">
            <?php foreach ($footer as $block): ?>
                <?php if ($block->type === 'footer-nav-element'): ?>
                    <a class="footer-link" href="<?php echo Url::to([$block->url]); ?>"><?= $block->content ?></a>
                <?php endif; ?>
            <?php endforeach; ?>
        </nav>

    </div>

</footer>

