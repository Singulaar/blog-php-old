<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model ContactForm */
/* @var $contact SiteController */

use frontend\controllers\SiteController;
use frontend\models\ContactForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner">
    <div class="section section-gray">
        <div class="section-head">
            <div class="container">
                <h1 class="section-title">Contact Us</h1>
                <div class="section-about">Let us know if you have any questions.</div>
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <?php $form = ActiveForm::begin(['id' => 'contact-form', 'class' => 'contacts']); ?>
                    <div class="form-group">
                        <label for="Email">Your Email Address</label>
                        <input type="email" class="form-control" id="Email" placeholder="something@website.com" required="">
                    </div>
                    <div class="form-group">
                        <label for="Subject">Subject</label>
                        <input type="email" class="form-control" id="Subject" placeholder="Question about your article" required="">
                    </div>
                    <div class="form-group">
                        <label for="Message">Message</label>
                        <textarea class="form-control" id="Message" placeholder="Your message starts with…" rows="4" required=""></textarea>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Send a Message', ['class' => 'btn btn-primary btn-block', 'name' => 'contact-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                <div class="center content">
                    <?php foreach ($contact as $block): ?>
                        <?php if ($block->type === 'text'): ?>
                            <?= $block->content ?>
                        <?php endif ?>
                        <?php if ($block->type === 'image-big'): ?>
                            <div class="figure-large">
                                <img src="../images/<?= $block->image ?>"/>
                                <figcaption>
                                    <?= $block->label ?>
                                </figcaption>
                            </div>
                        <?php endif ?>
                        <?php if ($block->type === 'quote'): ?>
                            <blockquote>
                                <?= $block->content ?>
                            </blockquote>
                        <?php endif ?>
                        <?php if ($block->type === 'tip'): ?>
                            <div class="tip">
                                <?= $block->content ?>
                                <img src="../images/<?= $block->image ?>"/>
                                <?= $block->label ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END inner -->
<!-- END inner -->
