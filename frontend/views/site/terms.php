<?php

/* @var $this yii\web\View */

/* @var $terms SiteController */

use frontend\controllers\SiteController;

$this->title = 'Terms';

?>
<div class="inner">
    <div class="section">
        <div class="section-head">
            <div class="container">
                <h1 class="section-title">Terms of Services</h1>
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <div class="center content">
                    <?php foreach ($terms as $block): ?>
                        <?php if ($block->type === 'text'): ?>
                            <?= $block->content ?>
                        <?php endif ?>
                        <?php if ($block->type === 'image-big'): ?>
                            <div class="figure-large">
                                <img src="../images/<?= $block->image ?>"/>
                                <figcaption>
                                    <?= $block->label ?>
                                </figcaption>
                            </div>
                        <?php endif ?>
                        <?php if ($block->type === 'quote'): ?>
                            <blockquote>
                                <?= $block->content ?>
                            </blockquote>
                        <?php endif ?>
                        <?php if ($block->type === 'tip'): ?>
                            <div class="tip">
                                <?= $block->content ?>
                                <img src="../images/<?= $block->image ?>"/>
                                <?= $block->label ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>