<meta name="theme-color" content="#fff">
<meta name="format-detection" content="telephone=no">
<link rel="apple-touch-icon" sizes="180x180" href="../../web/img/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../../web/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../../web/img/favicon-16x16.png">
<link rel="mask-icon" href="../../web/img/safari-pinned-tab.svg" color="#2a505b">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|PT+Serif:400,400i,700,700i"
      rel="stylesheet">
<meta name="description" content="">
<!-- Twitter Card data -->
<meta name="twitter:card" content="">
<meta name="twitter:site" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:creator" content="">
<meta name="twitter:image" content="">
<!-- Open Graph data -->
<meta property="og:title" content="">
<meta property="og:type" content="article">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta property="og:description" content="">
<meta property="og:site_name" content="">
<meta property="fb:admins" content="">
