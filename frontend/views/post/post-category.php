<?php

/* @var $this yii\web\View */
/* @var $post PostController */

/* @var $top_post PostController */

use frontend\controllers\PostController;
use yii\helpers\Html;
use yii\helpers\Url; ?>

<div class="inner">
    <div class="section section-blog">
        <div class="container">
            <div class="row">
                <?php foreach ($top_post as $item): ?>
                    <div class="col-lg-12 card card-featured card-top">
                        <a class="card-preview"
                           href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$item->id}") ?>">
                            <img class="card-pic" src="http://localhost/advanced/frontend/web/images/<?= $item->img ?>"
                                 alt="<?= $item->title ?>"/>
                        </a>
                        <div class="card-body">
                            <div class="card-meta"><?= $item->post_meta ?></div>
                            <h2 class="card-title">
                                <a href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$item->id}") ?>"><?= $item->title ?></a>
                            </h2>
                            <div class="card-text"><?= $item->short_description ?>
                            </div>
                            <div class="card-user user">
                                <div class="user-preview">
                                    <img class="user-pic"
                                         src="http://localhost/advanced/frontend/web/user-pic/<?= $item->author_pic ?>"
                                         alt="<?= $item->author_name ?>"/>
                                </div>
                                <div class="user-wrap">
                                    <div class="user-name"><?= $item->author_name ?></div>
                                    <div class="user-meta"><?= $item->author_meta ?> · <?= $item->read_time ?> min
                                        read
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($post as $item): ?>
                    <div class="col-lg-6 card">
                        <a class="card-preview"
                           href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$item->id}") ?>">
                            <img class="card-pic" src="http://localhost/advanced/frontend/web/images/<?= $item->img ?>"
                                 alt="<?= $item->title ?>"/>
                        </a>
                        <div class="card-body">
                            <div class="card-meta"><?= $item->read_time ?> · min read</div>
                            <h3 class="card-title">
                                <a href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$item->id}") ?>"><?= $item->title ?></a>
                            </h3>
                            <div class="card-text"><?= $item->short_description ?>
                            </div>
                            <div class="card-user user">
                                <div class="user-preview">
                                    <img class="user-pic"
                                         src="http://localhost/advanced/frontend/web/user-pic/<?= $item->author_pic ?>"
                                         alt="<?= $item->author_name ?>"/>
                                </div>
                                <div class="user-wrap">
                                    <div class="user-name"><?= $item->author_name ?></div>
                                    <div class="user-meta"><?= $item->author_meta ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
  
