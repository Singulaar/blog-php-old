<?php

/* @var $this yii\web\View */
/* @var $post PostController */
/* @var $id PostController */
/* @var $post_content PostController */
/* @var $side_post PostController */

/* @var $top_post PostController */

use frontend\controllers\PostController;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php foreach ($post as $item): ?>
<?php $this->title = $item->title; ?>
<div class="inner">
    <div class="section section-featured js-section-featured">
        <div class="container">
            <div class="card card-featured card-top m-0">
                <div class="card-preview">
                    <img class="card-pic" src="../images/<?= $item->img ?>" alt="<?= $item->title ?>"/>
                </div>
                <div class="card-body">
                    <div class="card-meta"><?= $item->post_meta ?></div>
                    <h1 class="card-title"><?= $item->title ?></h1>
                    <div class="card-text"><?= $item->short_description ?></div>
                    <div class="card-user user">
                        <div class="user-preview">
                            <img class="user-pic" src="../user-pic/<?= $item->author_pic ?>"
                                 alt="<?= $item->author_name ?>"/>
                        </div>
                        <div class="user-wrap">
                            <div class="user-name"><?= $item->author_name ?></div>
                            <div class="user-meta"><?= $item->author_meta ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <div class="section section-content">
        <div class="container">
            <div class="center">
                <div class="content">
                    <?php foreach ($post_content as $block): ?>
                        <?php if ($block->type === 'text'): ?>
                            <?= $block->content ?>
                        <?php endif ?>
                        <?php if ($block->type === 'image-big'): ?>
                            <div class="figure-large">
                                <img src="../images/<?= $block->image ?>"/>
                                <figcaption>
                                    <?= $block->label ?>
                                </figcaption>
                            </div>
                        <?php endif ?>

                        <?php if ($block->type === 'quote'): ?>
                            <blockquote>
                                <?= $block->content ?>
                                <?php foreach ($post as $item): ?>
                                    <div class="user">
                                        <div class="user-preview">
                                            <img class="user-pic" src="../user-pic/<?= $item->author_pic ?>"
                                                 alt="<?= $item->author_name ?>"/>
                                        </div>
                                        <div class="user-wrap">
                                            <div class="user-name"><?= $item->author_name ?></div>
                                            <div class="user-meta">, <?= $item->author_meta ?></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </blockquote>
                        <?php endif ?>

                        <?php if ($block->type === 'tip'): ?>
                            <div class="tip">
                                <?= $block->content ?>
                                <img src="../images/<?= $block->image ?>"/>
                                <?= $block->label ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach; ?>
                </div>
                <?php foreach ($post as $item): ?>
                    <div class="written js-written">
                        <div class="written-preview">
                            <img class="written-pic" src="../user-pic/<?= $item->author_pic ?>"
                                 alt="<?= $item->author_name ?>"/>
                        </div>
                        <div class="written-wrap">
                            <div class="written-label">Written By</div>
                            <div class="written-author"><?= $item->author_name ?></div>
                            <div class="written-about"><?= $item->author_meta ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="share share-post">
                    <div class="share-title">Spread your love, <strong>share the article</strong></div>
                    <div class="share-list">
                        <a href="#" class="share-btn share-twitter btn">
                            <img class="share-icon"
                                 src="http://localhost/advanced/frontend/web/img/icon-twitter-white.svg"
                                 alt="Share on Twitter">
                            <span class="share-label">Share on Twitter</span>
                        </a>
                        <a href="#" class="share-btn share-facebook btn">
                            <img class="share-icon"
                                 src="http://localhost/advanced/frontend/web/img/icon-facebook-white.svg"
                                 alt="Share on Facebook">
                            <span class="share-label">Share on Facebook</span>
                        </a>
                    </div>
                </div>
                <div id="disqus_thread"></div>
                <script> /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

                    var disqus_config = function () {
                            this.page.url = '<?php echo Yii::$app->request->getAbsoluteUrl() ?>';  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = '<?php echo $id ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };

                    (function () { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://parrot-blog-1.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                        powered by Disqus.</a></noscript>
                <script id="dsq-count-scr" src="//parrot-blog-1.disqus.com/count.js" async></script>
            </div>
        </div>
    </div>
    <div class="section section-more">
        <div class="container">
            <div class="row">
                <?php foreach ($top_post as $top): ?>
                    <div class="col-lg-12 card card-featured">
                        <a class="card-preview"
                           href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$top->id}") ?>">
                            <img class="card-pic" src="../images/<?= $top->img ?>" alt="<?= $top->title ?>"/>
                        </a>
                        <div class="card-body">
                            <div class="card-meta">Continue Reading</div>
                            <h3 class="card-title">
                                <a href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$top->id}") ?>"><?= $top->title ?></a>
                            </h3>
                            <div class="card-text"><?= $top->short_description ?>
                            </div>
                            <div class="card-user user">
                                <div class="user-preview">
                                    <img class="user-pic" src="../user-pic/<?= $top->author_pic ?>"
                                         alt="<?= $top->author_name ?>"/>
                                </div>
                                <div class="user-wrap">
                                    <div class="user-name"><?= $top->author_name ?></div>
                                    <div class="user-meta"><?= $top->author_meta ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($side_post as $side): ?>
                    <div class="col-md-6 col-lg-4 card">
                        <a class="card-preview"
                           href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$side->id}") ?>">
                            <img class="card-pic" src="../images/<?= $side->img ?>" alt="<?= $side->title ?>"/>
                        </a>
                        <div class="card-body">
                            <div class="card-meta"><a
                                        href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$side->id}") ?>"><?= $side->post_meta ?></a>
                                · <?= $side->read_time ?> min read
                            </div>
                            <h3 class="card-title">
                                <a href="<?= Url::to(['post/index']); ?>?id=<?= Html::encode("{$side->id}") ?>"><?= $side->title ?></a>
                            </h3>
                            <div class="card-text"><?= $side->short_description ?>
                            </div>
                            <div class="card-user user">
                                <div class="user-preview">
                                    <img class="user-pic" src="../user-pic/<?= $side->author_pic ?>"
                                         alt="<?= $side->author_name ?>"/>
                                </div>
                                <div class="user-wrap">
                                    <div class="user-name"><?= $side->author_name ?></div>
                                    <div class="user-meta"><?= $side->author_meta ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>



