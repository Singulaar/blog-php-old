// example of simple includes for js

// When the user scrolls the page, execute ProgressBar 
(function(){
	window.onscroll = function() {ProgressBar()};

	function ProgressBar() {
		var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
		var scrolled = (winScroll / height) * 100;
		document.querySelector(".js-indicator-position").style.width = scrolled + "%";
	}
}());


// Change header state
(function(){
	const header = $('.js-header-post');

	if (header.length) {
		var posLast = $(window).scrollTop();

		$(window).scroll(function(){
			var posNew = $(window).scrollTop(),
					featuredHeight = $('.js-section-featured').outerHeight(),
					writtenTop = $('.js-written').offset().top - $(window).height();

			if (posNew >= featuredHeight) {

				if (posNew > posLast) {
					header.addClass('header-state-article');
				} else {
					header.removeClass('header-state-article');
				}

			} else {
				header.removeClass('header-state-article');
			}

			if (posNew >= writtenTop) {
				header.addClass('header-state-share');
			} else {
				header.removeClass('header-state-share');
			}

			posLast = posNew;
		});
	}
}());