<?php

namespace frontend\controllers;

use frontend\models\Post;
use frontend\models\PostContent;
use yii\web\Controller;

class PostController extends Controller
{
    public function actionIndex($id)
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.id' => $id])->leftJoin('author', 'author.id = post.author_id')->all();

        $post_content = PostContent::find()->select(
            [
                'post_content.id',
                'post_content.post_id',
                'post_content.content',
                'post_content.type',
                'post_content.label',
                'post_content.image',
            ]
        )->where(['post_content.post_id' => $id])->orderBy(['id' => SORT_ASC])->all();

        $side_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 0])->andWhere(['!=', 'post.id', $id])->orderBy(['post.create_time' => SORT_DESC])->leftJoin('author', 'author.id = post.author_id')->limit(3)->all();

        $top_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 2])->leftJoin('author', 'author.id = post.author_id')->all();

        return $this->render('post',
            [
                'post' => $post,
                'id' => $id,
                'post_content' => $post_content,
                'top_post' => $top_post,
                'side_post' => $side_post,
            ]);
    }

    public function actionMore()
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->leftJoin('author', 'author.id = post.author_id')->orderBy(['post.create_time' => SORT_DESC])->all();

        return $this->render('seemore',
            [
                'post' => $post,
            ]);
    }

    public function actionArchitecture()
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.post_meta' => 'Architecture'])->leftJoin('author', 'author.id = post.author_id')->all();

        $top_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 2])->leftJoin('author', 'author.id = post.author_id')->all();

        return $this->render('post-category',
            [
                'post' => $post,
                'top_post' => $top_post,
            ]);
    }

    public function actionRecipes()
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.post_meta' => 'Recipes'])->leftJoin('author', 'author.id = post.author_id')->all();

        $top_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 2])->leftJoin('author', 'author.id = post.author_id')->all();

        return $this->render('post-category',
            [
                'post' => $post,
                'top_post' => $top_post,
            ]);
    }

    public function actionPhotography()
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.post_meta' => 'Photography'])->leftJoin('author', 'author.id = post.author_id')->all();

        $top_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 2])->leftJoin('author', 'author.id = post.author_id')->all();

        return $this->render('post-category',
            [
                'post' => $post,
                'top_post' => $top_post,
            ]);
    }

    public function actionFeaturedArticle()
    {
        $post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.post_meta' => 'Featured Article'])->leftJoin('author', 'author.id = post.author_id')->all();

        $top_post = Post::find()->select(
            [
                'post.id',
                'post.author_id',
                'post.title',
                'post.short_description',
                'post.read_time',
                'post.img',
                'post.create_time',
                'post.update_time',
                'post.post_meta',
                'post.status',
                'author.author_name',
                'author.author_pic',
                'author.author_meta',
            ])->where(['post.status' => 2])->leftJoin('author', 'author.id = post.author_id')->all();

        return $this->render('post-category',
            [
                'post' => $post,
                'top_post' => $top_post,
            ]);
    }
}
