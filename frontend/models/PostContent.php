<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;

/**
 * This is the model class for table "post_content".
 *
 * @property int $id
 * @property int|null $post_id
 * @property string|null $content
 * @property string|null $type
 * @property string|null $label
 * @property string|null $image
 */
class PostContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id'], 'integer'],
            [['content', 'label'], 'string'],
            [['type'], 'string', 'max' => 30],
            [['image'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'content' => 'Content',
            'type' => 'Type',
            'label' => 'Label',
            'image' => 'Image',
        ];
    }

    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'image',
                'folder' => '../../frontend/web/images'
            ],
        ];
    }
}
