<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;

/**
 * This is the model class for table "terms".
 *
 * @property int $id
 * @property string|null $content
 * @property string|null $type
 * @property string|null $label
 * @property string|null $image
 */
class Terms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'terms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 30],
            [['content'], 'string'],
            [['image'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'type' => 'Type',
            'label' => 'Label',
            'image' => 'Image',
        ];
    }
    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'image',
                'folder' => '../../frontend/web/images'
            ]
        ];
    }
}
