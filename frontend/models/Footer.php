<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;

/**
 * This is the model class for table "footer".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $content
 * @property string|null $url
 * @property string|null $img
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'url'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 30],
            [['img'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'content' => 'Content',
            'url' => 'Url',
            'img' => 'Img',
        ];
    }

    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'img',
                'folder' => '../../frontend/web/images'
            ]
        ];
    }
}
