<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "author".
 *
 * @property int $id
 * @property string|null $author_name
 * @property string|null $author_meta
 * @property string|null $author_pic
 */
class Author extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_name', 'author_meta',], 'string', 'max' => 30],
            [['author_meta',], 'string', 'max' => 255],
            [['author_pic'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_name' => 'Author Name',
            'author_meta' => 'Author Meta',
            'author_pic' => 'Author Pic',
        ];
    }

    public static function nameDropdown()
    {
        $author = self::find()->all();
        return ArrayHelper::map($author, 'id', 'author_name');
    }

    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'author_pic',
                'folder' => '../../frontend/web/user-pic'
            ]
        ];
    }
}
