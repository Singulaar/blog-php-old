<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\SubListBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "navbar".
 *
 * @property int $id
 * @property string|null $nav_block_name
 * @property string|null $nav_block_url
 */
class Navbar extends \yii\db\ActiveRecord
{
    public $nav_item_type;
    public $nav_item_url;
    public $nav_item_content;
    public $nav_item_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'navbar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nav_block_url'], 'string', 'max' => 255],
            [['nav_block_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nav_block_name' => 'Nav Block Name',
            'nav_block_url' => 'Nav Block Url',
        ];
    }

    public static function navigationIdDropdown()
    {
        $nav = self::find()->all();
        return ArrayHelper::map($nav, 'id', 'nav_block_name');
    }

    public function getNavItems()
    {
        return $this->hasMany(NavItem::className(), ['nav_item_id' => 'id'])->orderBy('nav_item_content');
    }

    public function behaviors()
    {
        return [
            'nav_item' => [
                'class' => SubListBehavior::class,
                'listId' => 'nav_item',
                'route' => 'nav-item/sub-list',
                'parentField' => 'nav_item_id'
            ]
        ];
    }
}
