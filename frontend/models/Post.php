<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;
use mrstroz\wavecms\components\behaviors\SubListBehavior;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int|null $author_id
 * @property string|null $title
 * @property string|null $short_description
 * @property string|null $read_time
 * @property string|null $img
 * @property string|null $create_time
 * @property string|null $update_time
 * @property string|null $post_meta
 * @property string|null $author
 * @property int|null $status
 */
class Post extends \yii\db\ActiveRecord
{
    public $author;
    public $author_name;
    public $author_meta;
    public $author_pic;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'read_time', 'status'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['title', 'post_meta'], 'string', 'max' => 255],
            [['short_description'], 'string'],
            [['img'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'read_time' => 'Read Time',
            'img' => 'Img',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'post_meta' => 'Post Meta',
            'status' => 'Status',
            'author' => 'Author',
        ];
    }

    public function getAuthorName()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'img',
                'folder' => '../../frontend/web/images'
            ],
            'post_content' => [
                'class' => SubListBehavior::class,
                'listId' => 'post_content',
                'route' => 'post-content/sub-list',
                'parentField' => 'post_id'
            ],
        ];
    }
}
