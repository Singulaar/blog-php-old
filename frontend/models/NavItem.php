<?php

namespace frontend\models;

use mrstroz\wavecms\components\behaviors\ImageBehavior;

/**
 * This is the model class for table "nav_item".
 *
 * @property int $id
 * @property int|null $nav_item_id
 * @property string|null $nav_item_content
 * @property string|null $nav_item_type
 * @property string|null $nav_item_img
 * @property string|null $nav_item_url
 */
class NavItem extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nav_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nav_item_id'], 'integer'],
            [['nav_item_content', 'nav_item_url'], 'string', 'max' => 255],
            [['nav_item_type'], 'string', 'max' => 30],
            [['nav_item_img'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nav_item_id' => 'Nav Item ID',
            'nav_item_content' => 'Nav Item Content',
            'nav_item_type' => 'Nav Item Type',
            'nav_item_img' => 'Nav Item Img',
            'nav_item_url' => 'Nav Item Url',
        ];
    }

    public function behaviors()
    {
        return [
            'img' => [
                'class' => ImageBehavior::class,
                'attribute' => 'nav_item_img',
                'folder' => '../../frontend/web/images'
            ],
        ];
    }
}
