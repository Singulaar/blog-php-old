'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');


sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('web/css'))
        .pipe(gulp.dest('./../backend/web/css'));
});

gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', gulp.series('sass'));
});

// gulp.task('js', function () {
//     return gulp
//         .src([
//             // './../node_modules/jquery/dist/jquery.js',
//             './../node_modules/bootstrap/dist/js/bootstrap.js',
//             'web/source/main.js',
//
//         ])
//         .pipe(concat('main.js'))
//         .pipe(gulp.dest('web/dist'))
//         .pipe(gulp.dest('./../backend/web/dist'));
//
// });


